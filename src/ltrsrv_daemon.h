#ifndef LTRSRV_DAEMON_H
#define LTRSRV_DAEMON_H

#include "ltrsrv_opt_parse.h"

extern int daemon_main(void);


int ltrsrv_daemon_start(void);
void ltrsrv_daemon_close(void);
int ltrsrv_daemon_svc_op(t_ltrd_svc_op op);



#endif // LTRSRV_DAEMON_H
