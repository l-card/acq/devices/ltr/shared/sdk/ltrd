#ifndef LTRSRV_CFG_H_
#define LTRSRV_CFG_H_


/** максимальное количество клиентов на один модуль */
#define LTRSRV_CLIENT_PER_MODULE_MAX  8


/** задержка в мс от прихода данных от клиента, до непосредственно попытки
 *  посылки данных. используется, чтобы дать возможность перемешать данные от
 *  разных модулей */
#define LTRSRV_CRATE_SEND_WAIT_TIME  0


/* размер буфера для приема за раз данных от крейта */
#define LTRSRV_ETH_RCV_BUF_SIZE   (64*1024)


/** размер общего буфера на передачу данных крейту в байтах */
#define LTRSRV_CRATE_SEND_BUF_SIZE           (16*1024*4)
/** размер буфера для каждого модуля на передачу данных в словах */
#define LTRSRV_MODULE_SEND_BUF_DEFAULT_SIZE    (128*4*1024)
/** размер буфера на прием данных для каждого модуля в словах */
#define LTRSRV_MODULE_RECV_BUF_DEFAULT_SIZE    (256*4*1024)

#define LTRSRV_MODULE_SEND_BUF_MIN_SIZE         1024
#define LTRSRV_MODULE_RECV_BUF_MIN_SIZE         1024

/** размер буфера для сохранения лога для вычитывания его через API */
#define LTRSRV_LOGPROTO_BUF_SIZE       (1024*1024)
/** максимальное количество клиентов, подключенных для чтения лога через API */
#define LTRSRV_LOGPROTO_CLIENT_CNT     16

/** Максимально возможное количество крейтов в системе */
#define LTRSRV_MAX_CRATES_CNT        64

/** Максимально возможное количество клиентов */
#define LTRSRV_CLIENT_COUNT_MAX  (LTRSRV_MAX_CRATES_CNT*LTRSRV_CLIENT_PER_MODULE_MAX*16)

/** Размер очереди сокетов на прием подключений клиентов */
#define LTRSRV_CLIENT_LISTEN_QUEUE_SIZE  16

/** Таймаут на установление соединения с сервером по TCP */
#define LTRSRV_ETH_CONNECTION_TOUT      5000
#define LTRSRV_ETH_CONNECTION_TOUT_MIN  1000

/** Таймаут на передачу управляющей команды по TCP */
#define LTRSRV_ETH_CTL_CMD_TOUT         5000
#define LTRSRV_ETH_CTL_CMD_TOUT_MIN     1000

/** Интервал переодического опроса крейта по TCP */
#define LTRSRV_ETH_CRATE_POLL_INTERVAL          1000
#define LTRSRV_ETH_CRATE_POLL_INTERVAL_MIN      1000

#define LTRSRV_ETH_RECONNECT_TOUT           3000
#define LTRSRV_ETH_RECONNECT_TOUT_MIN       1000

/** Максимальное количество статических записей IP */
#define LTRSRV_ETH_STATIC_IP_ENTRY_CNT      256

/** Максимальное кол-во адресов хост-машины, которое отслеживает ltr-сервер */
#define LTRSRV_ETH_HOST_ADDRS_MAX    16

/** Интервал проверки IP-адресов хоста */
#define LTR_SERVER_DEFAULT_ETH_INTF_CHECK_TIME      1000
#define LTR_SERVER_DEFAULT_ETH_INTF_CHECK_TIME_MIN  300

#define LTRSRV_BW_CHECK_TIME     500

/** Время на получения ответа на STOP-STOP-RESET-STOP в мс при инициализации модуля.
 *  Если ответ не будет получен, то будет попытка повтороного сброса модуля */
#define LTRSRV_MODULE_RST_DONE_CHECK_TIME           1500
/** Максимальное количество попыток сброса модуля из-за отсутствия ответа на сброс */
#define LTRSRV_MODULE_RST_RESTART_MAX_CNT           100

/** Время на получения ответа на запрос subid в мс при инициализации модуля.
 *  Если ответ не будет получен, то будет попытка повтороного запроса */
#define LTRSRV_MODULE_SUBID_DONE_CHECK_TIME          1500
/** Максимальное количество попыток запросить расширенный ID модуля */
#define LTRSRV_MODULE_SUBID_RESTART_MAX_CNT          5


/** максимальное кол-во ожидающих сокетов/событий/таймеров */
#define LTRSRV_WAIT_SOCK_MAX   (LTRSRV_CLIENT_COUNT_MAX + 20*LTRSRV_MAX_CRATES_CNT+1024)

#define LTRSRV_DEFAULT_LOG_FILE  ""


int ltrsrv_load_settings(const char* filename);
int ltrsrv_save_settings(void);
void ltrsrv_settings_mark_modified(void);

#endif

