/** @file ltrsrv_log.h
   Файл содержит описания функций для записи лога */

#ifndef LTRSRV_LOG_H
#define LTRSRV_LOG_H

#include "ltrsrv_crates.h"

/** уровни отладочных сообщений */
typedef enum {
    LTRSRV_LOGLVL_ERR_FATAL = 0,
    LTRSRV_LOGLVL_ERR       = 1,
    LTRSRV_LOGLVL_WARN      = 2,
    LTRSRV_LOGLVL_INFO      = 3,
    LTRSRV_LOGLVL_DETAIL    = 4,
    LTRSRV_LOGLVL_DBG_HIGH  = 5,
    LTRSRV_LOGLVL_DBG_MED   = 6,
    LTRSRV_LOGLVL_DBG_LOW   = 7
} t_ltrsrv_log_level;

typedef struct {
    int code;
    const char* str;
} t_ltrsrv_str_tbl;

#define LTRSRV_LOGSRC_SERVER            "Service"
#define LTRSRV_LOGSRC_CRATE             "Crate"
#define LTRSRV_LOGSRC_CRATE_INIT        "Crate init"
#define LTRSRV_LOGSRC_CLIENT            "Client"
#define LTRSRV_LOGSRC_CLIENT_INIT       "Client init"
#define LTRSRV_LOGSRC_CLIENT_LOGPROTO   "Client log con"

#ifdef _WIN32
    #define LTRSRV_LAST_SYSERR GetLastError()
#else
    #define LTRSRV_LAST_SYSERR errno
#endif



int ltrsrv_log_set_lvl(t_ltrsrv_log_level lvl, int permanent, int modify);
t_ltrsrv_log_level ltrsrv_log_get_save_lvl(void);
t_ltrsrv_log_level ltrsrv_log_get_cur_lvl(void);
int ltrsrv_log_set_file(const char* filename);
void ltrsrv_log_init(void);
void ltrsrv_log_close(void);
void ltrsrv_log_rec(t_ltrsrv_log_level lvl, const char *src, int err, const char *fmt, ...);
void ltrsrv_log_syserr(const char* src, int err, int syserr, const char* fmt, ...);

const char* ltrsrv_log_get_crate_iface_str(t_ltr_crate_interface iface);

int ltrsrv_log_make_crate_srcstr(t_ltr_crate* crate);
int ltrsrv_log_make_module_srcstr(t_ltr_crate* crate, int slot);
const char* ltrsrv_log_get_str(int code, const t_ltrsrv_str_tbl *tbl, int tbl_size);

#endif
