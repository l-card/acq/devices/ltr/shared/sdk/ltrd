/***************************************************************************//**
  @file ltrsrv_opt_parse.c
  @author Borisov Alexey <borisov@lcard.ru>
  @date   21.04.2012

  Файл содержит логику разбора опций командной строки.
  *****************************************************************************/
#include <stdlib.h>
  
#include "ltrsrv_opt_parse.h"
#include "ltrsrv_defs.h"
#include "getopt.h"
#include "config.h"

#define LTRD_OPT_CFGFILE                0x10000
#define LTRD_OPT_PIDFILE                0x10001
#define LTRD_OPT_USER                   0x10002
#define LTRD_OPT_GROUP                  0x10003
#define LTRD_OPT_DAEMON                 0x10004
#define LTRD_OPT_SYSTEMD_MODE           0x10005
#define LTRD_OPT_SVC_OP_INSTALL         0x10006
#define LTRD_OPT_SVC_OP_REMOVE          0x10007
#define LTRD_OPT_SVC_OP_START           0x10008
#define LTRD_OPT_SVC_OP_STOP            0x10009
#define LTRD_OPT_SVC_OP_AUTOSTART_EN    0x1000A
#define LTRD_OPT_SVC_OP_AUTOSTART_DIS   0x1000B

static const struct option f_long_opt[] = {
    {"config-file",         required_argument,       0, LTRD_OPT_CFGFILE},
    {"pid-file",            required_argument,       0, LTRD_OPT_PIDFILE},
    {"user",                required_argument,       0, LTRD_OPT_USER},
    {"group",               required_argument,       0, LTRD_OPT_GROUP},
    {"daemon",              no_argument,             0, LTRD_OPT_DAEMON},
    {"systemd-mode",        no_argument,             0, LTRD_OPT_SYSTEMD_MODE},
    {"install",             no_argument,             0, LTRD_OPT_SVC_OP_INSTALL},
    {"remove",              no_argument,             0, LTRD_OPT_SVC_OP_REMOVE},
    {"start",               no_argument,             0, LTRD_OPT_SVC_OP_START},
    {"stop",                no_argument,             0, LTRD_OPT_SVC_OP_STOP},
    {"autostart-enable",    no_argument,             0, LTRD_OPT_SVC_OP_AUTOSTART_EN},
    {"autostart-disable",   no_argument,             0, LTRD_OPT_SVC_OP_AUTOSTART_DIS},
    {0,0,0,0}
};

static const char* f_opt_str = "h";

int ltrsrv_parse_opt(t_ltrsrv_opts* opts, int argc, char** argv) {
    int err = 0;
    int opt = 0;

    while ((opt!=-1) && !err) {
        opt = getopt_long(argc, argv, f_opt_str, f_long_opt, 0);
        switch(opt) {
            case -1:
                break;
            case LTRD_OPT_CFGFILE:
                opts->cfg_file = optarg;
                break;
#ifdef LTRD_DAEMON_ENABLED
            case LTRD_OPT_PIDFILE:
                opts->pid_file = optarg;
                break;
#endif
            case LTRD_OPT_USER:
                opts->user = optarg;
                break;
            case LTRD_OPT_GROUP:
                opts->group = optarg;
                break;
#ifdef LTRD_DAEMON_ENABLED
            case LTRD_OPT_DAEMON:
                opts->mode = LTRD_MODE_DAEMON;
                break;
#ifdef _WIN32
            case LTRD_OPT_SVC_OP_INSTALL:
                opts->mode = LTRD_MODE_SVC_OP;
                opts->svc_op = LTRD_SVC_OP_INSTALL;
                break;
            case LTRD_OPT_SVC_OP_REMOVE:
                opts->mode = LTRD_MODE_SVC_OP;
                opts->svc_op = LTRD_SVC_OP_REMOVE;
                break;
            case LTRD_OPT_SVC_OP_START:
                opts->mode = LTRD_MODE_SVC_OP;
                opts->svc_op = LTRD_SVC_OP_START;
                break;
            case LTRD_OPT_SVC_OP_STOP:
                opts->mode = LTRD_MODE_SVC_OP;
                opts->svc_op = LTRD_SVC_OP_STOP;
                break;
            case LTRD_OPT_SVC_OP_AUTOSTART_EN:
                opts->mode = LTRD_MODE_SVC_OP;
                opts->svc_op = LTRD_SVC_OP_AUTOSTART_EN;
                break;
            case LTRD_OPT_SVC_OP_AUTOSTART_DIS:
                opts->mode = LTRD_MODE_SVC_OP;
                opts->svc_op = LTRD_SVC_OP_AUTOSTART_DIS;
                break;
#endif
#endif
#ifdef LTRD_SYSTEMD_ENABLED
            case LTRD_OPT_SYSTEMD_MODE: {
                    char* tout_env;
                    opts->mode = LTRD_MODE_SYSTEMD;
                    tout_env = getenv("WATCHDOG_USEC");
                    opts->wdt_tout =  tout_env ? atoi(tout_env) : 0;
                }
                break;
#endif

            default:
                err  = LTRSRV_ERR_PARSE_OPTION;
                break;
        }
    }
    return err;
}


