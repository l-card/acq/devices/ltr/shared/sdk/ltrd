#ifndef LTR_CRATE_DEFS_H
#define LTR_CRATE_DEFS_H

#include "ltrsrv_defs.h"
#include "ltrsrv_cfg.h"
#include "lcspec.h"

// описание крейта (для TCRATE_INFO)
typedef enum {
    LTR_CRATE_TYPE_UNKNOWN         = 0,
    LTR_CRATE_TYPE_LTR010          = 10,
    LTR_CRATE_TYPE_LTR021          = 21,
    LTR_CRATE_TYPE_LTR030          = 30,
    LTR_CRATE_TYPE_LTR031          = 31,
    LTR_CRATE_TYPE_LTR032          = 32,
    LTR_CRATE_TYPE_LTR_CU_1        = 40,
    LTR_CRATE_TYPE_LTR_CEU_1       = 41,

    LTR_CRATE_TYPE_BOOTLOADER      = 99
} t_ltr_crate_type;

typedef enum {
    LTR_CRATE_IFACE_UNKNOWN        = 0,
    LTR_CRATE_IFACE_USB            = 1,
    LTR_CRATE_IFACE_TCPIP          = 2
} t_ltr_crate_interface;



#define LTR_MID_EMPTY                 0x0000
#define LTR_MID_IDENTIFYING           0xFFFF
#define LTR_MID_INVALID               0xFFFE
#define LTR_MID_LTR01                 0x0101
#define LTR_MID_LTR11                 0x0B0B        // идентификатор модуля LTR11
#define LTR_MID_LTR21                 0x1515
#define LTR_MID_LTR22                 0x1616        // идентификатор модуля LTR22
#define LTR_MID_LTR24                 0x1818
#define LTR_MID_LTR25                 0x1919
#define LTR_MID_LTR27                 0x1B1B        // идентификатор модуля LTR27
#define LTR_MID_LTR34                 0x2222        // идентификатор модуля LTR34
#define LTR_MID_LTR35                 0x2323
#define LTR_MID_LTR41                 0x2929        // идентификатор модуля LTR41
#define LTR_MID_LTR42                 0x2A2A        // идентификатор модуля LTR42
#define LTR_MID_LTR43                 0x2B2B        // идентификатор модуля LTR43
#define LTR_MID_LTR51                 0x3333        // идентификатор модуля LTR51
#define LTR_MID_LTR114                0x7272        // идентификатор модуля LTR114
#define LTR_MID_LTR210                0xD2D2        // идентификатор модуля LTR210
#define LTR_MID_LTR212                0xD4D4        // идентификатор модуля LTR212



/* коды, определяющие, к чему относится данный байт потока от крейта */
#define LTR_CRATE_STREAM_TYPE_DATA                (0x0000l)
#define LTR_CRATE_STREAM_TYPE_USERDATA            (0x4000l)
#define LTR_CRATE_STREAM_TYPE_CMD                 (0x8000l)
#define LTR_CRATE_STREAM_TYPE_CRATE_CMD           (0xC000l)
#define LTR_CRATE_STREAM_TYPE_Msk                 (0xC000l)

#define LTR_CRATE_STREAM_CMD_CODE_Msk      (LTR_CRATE_STREAM_TYPE_Msk | 0x30FF)
#define LTR_CRATE_STREAM_CMD_CODE_MARK     (LTR_CRATE_STREAM_TYPE_CMD | 0)

/* коды команд для потока от крейта */
#define LTR_CRATE_STREAM_CMD_Msk             (LTR_CRATE_STREAM_TYPE_Msk|0xC0)
#define LTR_CRATE_STREAM_CMD_STOP            (LTR_CRATE_STREAM_TYPE_CMD|0x00)
#define LTR_CRATE_STREAM_CMD_PROGR           (LTR_CRATE_STREAM_TYPE_CMD|0x40)
#define LTR_CRATE_STREAM_CMD_RESET           (LTR_CRATE_STREAM_TYPE_CMD|0x80)
#define LTR_CRATE_STREAM_CMD_INSTR           (LTR_CRATE_STREAM_TYPE_CMD|0xC0)


#define LTR_CRATE_STREAM_SLOT_Pos           (8UL)
#define LTR_CRATE_STREAM_SLOT_Msk           (0xFUL<<LTR_CRATE_STREAM_SLOT_Pos)

#define LTR_CRATE_STREAM_SLOT(slot)         ((slot << LTR_CRATE_STREAM_SLOT_Pos) \
                                            & LTR_CRATE_STREAM_SLOT_Msk)

#define LTR_CRATE_STREAM_GET_SLOT(wrd)         (((wrd) & LTR_CRATE_STREAM_SLOT_Msk) \
                                                  >> LTR_CRATE_STREAM_SLOT_Pos)

#define LTR_CRATE_STREAM_GET_CMD_DATA(wrd)         (((wrd) >> 16) & 0xFFFF)


/* флаги для синхрометок */
#define LTR_CRATE_STREAM_MARK_START             (0x1000)
#define LTR_CRATE_STREAM_MARK_SECOND            (0x2000)
#define LTR_CRATE_STREAM_MARK_GET_SEQ(wrd)      ((wrd>>14) & 0x3)


// распределение адресного пространства LTR010
#define SEL_AVR_DM                              0x82000000
#define SEL_AVR_PM                              0x83000000
#define SEL_DMA_TEST_FLAG                       0x84000000
#define SEL_FLASH_BUFFER                        0x85000000
#define SEL_FPGA_DATA                           0x86000000
#define SEL_FPGA_FLAGS                          0x87000000
#define SEL_FLASH_DSP                           0x88000000
#define SEL_FLASH_STATUS                        0x89000000
#define SEL_SDRAM                               0x8A000000
#define SEL_SRAM                                0x8B000000
#define SEL_DSP                                 0x90000000
#define SEL_DSP_SPI                             0x91000000
#define SEL_DSP_SPI_BOOT                        0x92000000
#define SEL_DSP_MEM                             0x93000000
#define SEL_SLOTS                               0x95000000



/* коды исключительных ситуаций */
#define LTR_CRATE_STREAM_EXCEPT_FAULT        0
#define LTR_CRATE_STREAM_EXCEPT_MODULES_CH   1
#define LTR_CRATE_STREAM_EXCEPT_BUF_OVFL     2
#define LTR_CRATE_STREAM_EXCEPT_BUF_MISS     3
#define LTR_CRATE_STREAM_EXCEPT_UNKNOWN_SYNC 4



#define LTR_CRATE_STREAM_EXCEPT_SIGN       0xFFFFFFFFUL
#define LTR_CRATE_STREAM_EXCEPT_SIGN2      0xFFFFAA00UL
#define LTR_CRATE_STREAM_EXCEPT_SIGN2_Msk  0xFFFFFF00UL

#define LTR_CRATE_STREAM_EXCEPT_CODE_Msk   0x000000FFUL

//

#define LTR_CRATE_STREAM_CRATE_CMD_MARK   0xFF10
#define LTR_CRATE_STREAM_CRATE_CMD_IRIG   0xFE00
#define LTR_CRATE_STREAM_MODULE_CMD_MARK  0x0



#define LTR_CRATE_ADDR_FIRMWARE_VER             (SEL_AVR_PM|0x002FF0)
#define LTR_CRATE_ADDR_BOOTLOADER_VER           (SEL_AVR_PM|0x003FF0)
#define LTR_CRATE_ADDR_MODULE_DESCR             (SEL_AVR_PM|0x003000)
#define LTR_CRATE_ADDR_SYSREGS                  (SEL_AVR_DM|0x008000)
#define LTR_CRATE_ADDR_BAUDRATE                 (SEL_AVR_DM|0x008006)
#define LTR_CRATE_ADDR_MODULES                  (SEL_AVR_DM|0x008008)
#define LTR_CRATE_ADDR_CONNECTION_PRIMARY       (SEL_AVR_DM|0x010070)
#define LTR_CRATE_ADDR_CONNECTION_FORCE         (SEL_AVR_DM|0x010072)
#define LTR_CRATE_ADDR_SLOT_CONFIG_VER          (SEL_AVR_DM|0x010101)
#define LTR_CRATE_ADDR_BOARD_REV                (SEL_DSP   |0x008001)
#define LTR_CRATE_ADDR_PRIMARY_IFACE            (SEL_DSP   |0x005004)

#define LTR_CRATE_ADDR_FPGA_STATE               (SEL_FPGA_DATA | 0x0001)


#define LTR_CRATE_REG_BITMSK_SLOTS16            (1<<6)


#define LTR_CRATE_ADDR_FIRMWARE_START          (SEL_AVR_PM|0x0000)
#define LTR_CRATE_ADDR_BOOTLOADER_START        (SEL_AVR_PM|0x3C00)
#define LTR_CRATE_ADDR_FPGA_FIRMWARE            (SEL_FPGA_DATA)

#define LTR_CRATE_ADDR_SLOT_HDR(slot)          (SEL_SLOTS | ((slot&0xFF)<<16))
#define LTR_CRATE_SLOT_HDR_SIZE                6

#define LTR_CRATE_SYSREGS_SIZE                  16


#define LTR_CRATE_REG_NAME_SIZE                 16
#define LTR_CRATE_REG_FIRMWARE_VER_SIZE         4
#define LTR_CRATE_REG_BOOTVER_VER_SIZE          4
#define LTR_CRATE_REG_BOARD_REV_SIZE            1
#define LTR_CRATE_REG_SLOT_CONFIG_VER_SIZE      2
#define LTR_CRATE_REG_CONNECTION_PRIMARY_SIZE   1
#define LTR_CRATE_REG_FPGA_STATE_SIZE           2
#define LTR_CRATE_REG_PRIMARY_IFACE_SIZE        1

#include "lcspec_pack_start.h"
/** дескриптор с данными о крейте в том виде, как иы его получаем от крейта */
typedef struct {
    uint8_t Name[16];
    uint8_t SerialNumber[16];
    uint8_t CpuType[16];
    uint8_t Revision;
    uint32_t QuartzFrequency;
    uint8_t Unused[128-53];
} LATTRIBUTE_PACKED t_ltr_device_raw_descr;
#include "lcspec_pack_restore.h"

#endif
