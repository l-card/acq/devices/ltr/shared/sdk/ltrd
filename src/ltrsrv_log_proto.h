#ifndef LTRSRV_LOG_PROTO_H
#define LTRSRV_LOG_PROTO_H

#include "ltrsrv_log.h"

void ltrsrv_logproto_close(void);

void ltrsrv_logproto_add_rec(t_ltrsrv_log_level lvl, int err,
                             const char *fmt, ...);

int ltrsrv_logproto_notify_data_rdy(struct st_ltr_client *client);
int ltrsrv_logproto_add_client(struct st_ltr_client *client);

#endif // LTRSRV_LOG_PROTO_H
