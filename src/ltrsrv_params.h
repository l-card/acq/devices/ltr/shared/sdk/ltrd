#ifndef LTRSRV_PARAMS_H
#define LTRSRV_PARAMS_H

#include <stdint.h>

#define LTRD_PARAM_GROUP_IS_ETH(val)   ((val & 0xFF00) == 0x100)
#define LTRD_PARAM_GROUP_IS_CRATE(val) ((val & 0xFF00) == 0x200)

typedef enum {
    /** Интервал опроса крейта, для проверки, рабочее ли подключение (в мс) */
    LTRD_PARAM_ETH_CRATE_POLL_TIME   = 0x100,
    /** Таймаут на установление соединения с крейтом (в мс) */
    LTRD_PARAM_ETH_CRATE_CON_TOUT    = 0x101,
    /** Таймаут на ответ на управляющую команду от крейта (в мс) */
    LTRD_PARAM_ETH_CRATE_CTLCMD_TOUT = 0x102,
    /** Интервал проверки адресов хоста для запуска автоподключения (в мс) */
    LTRD_PARAM_ETH_INTF_CHECK_TIME   = 0x103,
    /** Время на повторное подключения крейта по Ethernet после ошибки (в мс) */
    LTRD_PARAM_ETH_RECONNECTION_TIME = 0x104,
    /** Немедленная передача данных крейту (отключение алгоритма нейгла) */
    LTRD_PARAM_ETH_SEND_NODELAY      = 0x105,


    LTRD_PARAM_MODULE_SEND_BUF_SIZE  = 0x200,
    LTRD_PARAM_MODULE_RECV_BUF_SIZE  = 0x201
} t_ltrd_params;

int ltrsrv_param_put_uint32(void *val, uint32_t* size, uint32_t set_val);
int ltrsrv_param_get_uint32(const void *val, const uint32_t size, uint32_t *intval);


int ltrsrv_set_param(t_ltrd_params param, const void *val, uint32_t size);
int ltrsrv_get_param(t_ltrd_params param, void *val, uint32_t* size);

#endif // LTRSRV_PARAMS_H
