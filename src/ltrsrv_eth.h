#ifndef LTRSRV_ETH_H
#define LTRSRV_ETH_H

#include <stdint.h>
#include "ltrsrv_crates.h"
#include "ltrsrv_params.h"


typedef enum {
    LTR_CRATE_IP_FLAG_AUTOCONNECT = 0x00000001, /** признак, что нужно пробовать
                                                   соединится по этой записи при
                                                   старте и при ltrsrv_eth_start_autocon() */
    LTR_CRATE_IP_FLAG_RECONNECT   = 0x00000002, /** Признак, что нужно выполнять повторное
                                                    подключение в случае ошибки */
} t_ltr_crate_ip_flags;


typedef enum {
    LTR_CRATE_IP_STATUS_OFFLINE     = 0,
    LTR_CRATE_IP_STATUS_CONNECTING  = 1,
    LTR_CRATE_IP_STATUS_ONLINE      = 2,
    LTR_CRATE_IP_STATUS_ERROR       = 3
} t_ltr_crate_ip_status;

typedef struct {
    uint32_t ip_addr;
    uint32_t flags;
    char serial_number[16];
    uint8_t is_dynamic;
    uint8_t status;
} t_ltr_crate_client_ip_entry;


int ltrsrv_eth_init(void);
int ltrsrv_eth_close(void);

int ltrsrv_eth_start_autocon(void);

int ltrsrv_eth_get_ip_entry_list(t_ltr_crate_client_ip_entry* lst, int max_cnt, uint32_t msk, uint32_t addr, uint32_t *fnd_cnt);
int ltrsrv_eth_add_ip_entry(uint32_t addr, uint32_t flags, int permanent, int modify);
int ltrsrv_eth_rem_ip_entry(uint32_t addr, int permanent, int modify);
int ltrsrv_eth_ip_conn(uint32_t addr);
int ltrsrv_eth_ip_disc(uint32_t addr);
int ltrsrv_eth_ip_disc_all(void);
int ltrsrv_eth_ip_set_flags(uint32_t addr, uint32_t flags, int permanent, int modify);

int ltrsrv_get_next_saved_ip_entry(int start_ind, t_ltr_crate_client_ip_entry* entry);

#endif
