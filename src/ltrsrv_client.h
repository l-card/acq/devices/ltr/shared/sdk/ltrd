#ifndef LTRSRV_CLIENT_H
#define LTRSRV_CLIENT_H

#include "ltrsrv_defs.h"
#include "ltrsrv_crates.h"






#define LTR_SIG_ESC_MASK          (0x0000FFF0)  // маска esc-последовательности сигнала сервер->клиент
#define LTR_SIG_ESC_VAL           (0x0000FFF0)  // значение esc-последовательности сигнала сервер->клиент
#define LTR_SIG_MAX_DWORDS        0x0F
#define LTR_SIG_LEN(n)            ((n) & 0x0F)
#define LTR_SIG_TYPE_MASK         (0xFFFF0000)
// Если (data_word & LTR_SIG_ESC_MASK) == LTR_SIG_ESC_VAL, то изъять из потока
//      LTR_SIG_LEN(data_word) слов и обработать их отдельно.
#define LTR_SIG_TYPE_LEGACY       (0xFFFF0000)
#define LTR_SIG_TYPE_TSTAMP_EX    (0xFFFE0000)

#define LTR_SIG_STAMP_EX_SIZE     4

#define LTR_SIGNAL(type, len)                (LTR_SIG_ESC_VAL | ((type) & LTR_SIG_TYPE_MASK) | LTR_SIG_LEN(len))
// Готовые значения ESC-слов сигналов
#define LTR_CLIENT_STREAM_SIG_RBUF_OVF       LTR_SIGNAL(LTR_SIG_TYPE_LEGACY, 0)
#define LTR_CLIENT_STREAM_SIG_TIMESTAMP      LTR_SIGNAL(LTR_SIG_TYPE_LEGACY, 1)




typedef int (*t_ltr_client_snd_rdy_cb)(struct st_ltr_client *client);

int ltrsrv_clients_init(void);
void ltrsrv_clients_close(void);


void ltrsrv_client_close(struct st_ltr_client *client);


t_ltr_crate* ltrsrv_client_crate(struct st_ltr_client *client);
const char *ltrsrv_client_log_str(struct st_ltr_client *client);

/** Оповещение клиента, что модуль, с которым он был соединен, удален */
int ltrsrv_client_notify_mod_closed(struct st_ltr_client *client);



/** Оповещение клиентов, что крейт был удален (в первую очередь для управляющих
    каналов, соединенных с определенным крейтом) */
int ltrsrv_client_notify_crate_closed(t_ltr_crate *crate);

/** оповещение клиента, что его конкретный на прием данных от модуля переполнился,
   т.к. он не успел за другими клиентами. Только для мультиклиентного соедининия */
int ltrsrv_client_sndbuf_ov(struct st_ltr_client *client);

int ltrsrv_client_get_words(struct st_ltr_client *client, uint32_t *wrds, int rcv_size);
int ltrsrv_client_put_words(struct st_ltr_client *client, const uint32_t *wrds, int snd_size);
int ltrsrv_client_put_bytes(struct st_ltr_client *client, const uint8_t *bytes, int put_size);
int ltrsrv_client_get_bytes(struct st_ltr_client *client, uint8_t *bytes, int rcv_size);
int ltrsrv_client_recv_resume(struct st_ltr_client *client);
int ltrsrv_client_recv_stop(struct st_ltr_client *client);
int ltrsrv_client_wait_snd_rdy(struct st_ltr_client *client, t_ltr_client_snd_rdy_cb cb);
void ltrsrv_client_close_req(struct st_ltr_client *client, int err);


#endif
