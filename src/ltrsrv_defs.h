#ifndef LTRSRV_DEFS_H
#define LTRSRV_DEFS_H

#include <stdint.h>


typedef enum {
    LTRSRV_ERR_SUCCESS               = 0,
    LTRSRV_ERR_MEMALLOC              = -1,    
    LTRSRV_ERR_LOG_FILE_OPEN         = -2,
    LTRSRV_ERR_NO_FREE_CRATE_SLOT    = -3,
    LTRSRV_ERR_NO_FREE_SOCK          = -4, /** нет места, чтобы поставить сокет в очередь ожидания */
    LTRSRV_ERR_DAEMONIZE_ERROR       = -5,
    LTRSRV_ERR_PARSE_OPTION          = -6,
    LTRSRV_ERR_SIGNAL_RESET          = -7,
    LTRSRV_ERR_ALREADY_RUNNING       = -8,
    LTRSRV_ERR_CREATE_PIDFILE        = -9,
    LTRSRV_ERR_CLOSE_DESCR           = -10,
    LTRSRV_ERR_CREATE_PIPE           = -11,
    LTRSRV_ERR_FIND_USER             = -12,
    LTRSRV_ERR_FIND_GROUP            = -13,
    LTRSRV_ERR_CHANGE_GROUP_LIST     = -14,
    LTRSRV_ERR_CHANGE_UID            = -15,
    LTRSRV_ERR_CHANGE_GID            = -16,
    LTRSRV_ERR_NO_FREE_LOGPROTO_CLIENTS = -17,
    LTRSRV_ERR_CREATE_EVENT          = -18,
    LTRSRV_ERR_EVENT_SELECT          = -19,    





    LTRSRV_ERR_CFGFILE_PARSE         = -20,
    LTRSRV_ERR_CFGFILE_ROOT_ELEM     = -21,
    LTRSRV_ERR_SAVECFG_XML_NODE      = -22,
    LTRSRV_ERR_SAVECFG_FILE          = -23,
    LTRSRV_ERR_INVALID_LOG_LEVEL     = -24,
    LTRSRV_ERR_INVALID_PARAM         = -25,
    LTRSRV_ERR_INVALID_PARAM_SIZE    = -26,
    LTRSRV_ERR_INVALID_SVC_OP        = -27,
    LTRSRV_ERR_RESOURCE_ALLOC        = -28,
    LTRSRV_ERR_DEVNOTIFY_REGISTER    = -29,


    LTRSRV_ERR_SOCKET_CREATE         = -100, /** ошибка создания сокета */
    LTRSRV_ERR_SOCKET_ADDR_FAMILY    = -101,
    LTRSRV_ERR_SOCKET_INET_ADDR      = -102,
    LTRSRV_ERR_SOCKET_SET_NBLOCK     = -103, /** ошибка перевода сокета в неблокирующий режим */
    LTRSRV_ERR_SOCKET_CONNECT        = -104, /** ошибка на стадии попытки установить соединение */
    LTRSRV_ERR_SOCKET_SEND           = -105, /** ошибка передачи по сокету */
    LTRSRV_ERR_SOCKET_RECV           = -106,
    LTRSRV_ERR_SOCKET_SELECT         = -107, /** ошибка выполнения select() */
    LTRSRV_ERR_SOCKET_ABORTED        = -108, /** пришло ислючение по сокету */
    LTRSRV_ERR_SOCKET_CLOSED         = -109, /** другая сторона закрыла сокет */
    LTRSRV_ERR_SOCKET_BIND           = -110,
    LTRSRV_ERR_SOCKET_LISTEN         = -111,
    LTRSRV_ERR_SOCKET_ACCEPT         = -112,
    LTRSRV_ERR_SOCKET_SET_BUF_SIZE   = -113,
    LTRSRV_ERR_SOCKET_CON_RESET      = -114,
    LTRSRV_ERR_SOCKET_GEN_ERR        = -115,
    LTRSRV_ERR_SOCKET_HOST_UNREACH   = -116,
    LTRSRV_ERR_SOCKET_CON_REFUSED    = -117,
    LTRSRV_ERR_SOCKET_SET_NODELAY    = -118,




    LTRSRV_ERR_CLIENT_HANDLE                = -200,
    LTRSRV_ERR_CLIENT_INVALID_INIT_CMD      = -201,
    LTRSRV_ERR_CLIENT_CMD_SIGN              = -202,
    LTRSRV_ERR_CLIENT_UNSUP_CMD_CODE        = -203,
    LTRSRV_ERR_CLIENT_CRATE_NOT_FOUND       = -204,
    LTRSRV_ERR_CLIENT_CMD_OVERRUN           = -205,
    LTRSRV_ERR_CLIENT_CMD_UNSUP_FOR_SRV_CTL = -206,
    LTRSRV_ERR_CLIENT_CMD_PARMS             = -207,
    LTRSRV_ERR_CLIENT_CLOSED                = -208,



    LTRSRV_ERR_CRATE_HANDLE                 = -300,
    LTRSRV_ERR_CRATE_INTF_CLOSED            = -301,
    LTRSRV_ERR_CRATE_INVALID_INTF           = -302,
    LTRSRV_ERR_CRATE_FPGA_INFO_SIZE         = -303, /** неверный размер данных о FPGA */
    LTRSRV_ERR_CRATE_FPGA_INFO_FORMAT       = -304, /** неверный формат данных о FPGA - нет E0 в старшем байте */
    LTRSRV_ERR_CRATE_RAW_DESRC_SIZE         = -305, /** неверный размер принятого дескриптора от крейта */
    LTRSRV_ERR_CRATE_FIRMVARE_VER_SIZE      = -306,
    LTRSRV_ERR_CRATE_BOOTLOADER_VER_SIZE    = -307,
    LTRSRV_ERR_CRATE_CMD_IN_PROGRESS        = -308,
    LTRSRV_ERR_CRATE_CRATE_NOT_INIT         = -309,
    LTRSRV_ERR_CRATE_CMD_REPLY_LEN          = -310,
    LTRSRV_ERR_CRATE_CMD_QUEUE_FULL         = -311,
    LTRSRV_ERR_CRATE_INVALID_CMD_DATA_SIZE  = -312,
    LTRSRV_ERR_CRATE_INVALID_CMD_IN_QUEUE   = -313,
    LTRSRV_ERR_CRATE_UNEXPECTED_CMD_CB      = -314,
    LTRSRV_ERR_CRATE_UNREGISTRED            = -315,
    LTRSRV_ERR_CRATE_UNKNOWN_DEVNAME        = -316,
    LTRSRV_ERR_CRATE_SEND_BUF_OVFL          = -317,
    LTRSRV_ERR_CRATE_SLOT_EMPTY             = -318,
    LTRSRV_ERR_CRATE_INVALID_SLOT_NUM       = -319,
    LTRSRV_ERR_CRATE_INVALID_FIRM_FILE      = -320,
    LTRSRV_ERR_CRATE_FIRM_FILE_READ         = -321,
    LTRSRV_ERR_CRATE_BOARD_REV_SIZE         = -322,
    LTRSRV_ERR_CRATE_SLOTS_CONFIG_VER_SIZE  = -323,
    LTRSRV_ERR_CRATE_THERM_OUT_OF_RANGE     = -324,
    LTRSRV_ERR_CRATE_PRIMARY_IFACE_SIZE     = -325,
    LTRSRV_ERR_CRATE_UNEXPECTED_CTL_DATA    = -326,
    LTRSRV_ERR_CRATE_DATA_RECON_EXCEEDED    = -327,
    LTRSRV_ERR_CRATE_CLOSING                = -328,
    LTRSRV_ERR_CRATE_INSUF_PERMISSON        = -329,

    LTRSRV_ERR_MODULE_MID                   = -400,
    LTRSRV_ERR_MODULE_IN_USE                = -401,
    LTRSRV_ERR_MODULE_EXCEED_MAX_CLIENT     = -402,


    LTRSRV_ERR_ETH_CMD_EVENT         = -500, /** пришло неверное событие при обработке команды */
    LTRSRV_ERR_ETH_REPLY_SIGNATURE  = -501, /** неверный признак ответа для протокола по TCP */
    LTRSRV_ERR_ETH_REPLY_RESULT     = -502, /** в ответе на управляющую команду TCP
                                                код рузультата выполнения не равен 0 */
    LTRSRV_ERR_ETH_REPLY_SIZE       = -503, /** размер ответа команды по TCP превышает запрошенный размер */
    LTRSRV_ERR_ETH_INVALID_CMD       = -504, /** неверный код команды по Ethernet */
    LTRSRV_ERR_ETH_CMD_RECV_TOUT     = -505, /** произошел таймаут при выполнении команды */
    LTRSRV_ERR_ETH_CMD_SEND_TOUT           = -506, /** произошел таймаут при выполнении команды */
    LTRSRV_ERR_ETH_CONNECTION_TOUT         = -507,
    LTRSRV_ERR_ETH_EXCEED_MAX_IP_ENTRY_CNT = -508,
    LTRSRV_ERR_ETH_INVALID_IP_ENTRY        = -509,
    LTRSRV_ERR_ETH_CRATE_CONNECTED         = -510,

    LTRSRV_ERR_USB_OPEN_DEVICE             = -600, /**< Ошибка открытия USB-устройства */
    LTRSRV_ERR_USB_TRANSFER_FAILED         = -601, /**< Передача по USB завершена с ошибокй */
    LTRSRV_ERR_USB_TRANSF_BUSY             = -602, /**< Нет свободных передач по USB */
    LTRSRV_ERR_USB_TRANSF_OUT_OF_ORDER     = -603, /**< Трансфер завершен не в порядке постановления в очередь */
    LTRSRV_ERR_USB_START_TRANSFER          = -604,
    LTRSRV_ERR_USB_INVALID_CONFIGURATION   = -605, /**< Наверная конфигурация модуля или ее не удалось получить */
    LTRSRV_ERR_USB_IOCTL_FAILD             = -606,
    LTRSRV_ERR_USB_TRANSF_TOUT             = -607,
    LTRSRV_ERR_USB_INIT                    = -608
} t_ltrd_errs;




#ifndef MIN
    #define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

#ifndef MAX
    #define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif



/** Функция запрашивает завершение всех процессов в сервере */
void ltrsrv_req_server_close(void);
/** Функция запрашивает перезагрузку сервера с разрывом всех соединений */
void ltrsrv_req_server_restart(void);




#endif
