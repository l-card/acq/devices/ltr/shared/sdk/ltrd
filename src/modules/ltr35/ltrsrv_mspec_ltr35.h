#ifndef LTRSRV_MSPEC_LTR35_H
#define LTRSRV_MSPEC_LTR35_H

#include "ltrsrv_crates.h"

int ltrsrv_ltr35_init(t_ltr_module *module, uint32_t init_flags);

#endif // LTRSRV_MSPEC_LTR35_H
