#ifndef LTRSRV_MSPEC_LTR01_H
#define LTRSRV_MSPEC_LTR01_H

#include "ltrsrv_crates.h"

#define LTR01_CMD_SUBID                         (LTR_CRATE_STREAM_CMD_INSTR | 0x00)

int ltrsrv_ltr01_init(t_ltr_module *module, uint32_t init_flags);

#endif // LTRSRV_MSPEC_LTR01_H
