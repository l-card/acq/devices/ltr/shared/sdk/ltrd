#ifndef LTRSRV_OPT_PARSE_H
#define LTRSRV_OPT_PARSE_H

#include "config.h"

typedef enum {
    LTRD_MODE_CONSOLE = 0, /**< Работа как обычная консольная программа */
    LTRD_MODE_DAEMON  = 1, /**< Работа в режиме демона */
    LTRD_MODE_SYSTEMD = 2, /**< Работа в режиме сервиса systemd */
    LTRD_MODE_SVC_OP  = 3  /**< Выполнение дполнительной сервисной операции */
} t_ltrd_mode;


typedef enum {
    LTRD_SVC_OP_INSTALL         = 1, /**< Установка сервиса */
    LTRD_SVC_OP_REMOVE          = 2, /**< Удаление сервиса */
    LTRD_SVC_OP_START           = 3, /**< Запуск сервиса */
    LTRD_SVC_OP_STOP            = 4, /**< Останов сервиса */
    LTRD_SVC_OP_AUTOSTART_EN    = 5, /**< Разрешение автозапуска */
    LTRD_SVC_OP_AUTOSTART_DIS   = 6  /**< Запрет автозапуска */
} t_ltrd_svc_op;



/** Опции, переданные в командной строке */
typedef struct {
    t_ltrd_mode mode; /**< признак, что нужно запускать приложение в режиме демона */
    t_ltrd_svc_op svc_op; /**< сервисная операция для mode = LTRD_MODE_SVC_OP */
    const char* cfg_file; /**< конфигурационный файл */
    const char* user; /**< имя пользователя, привилегии которого нужно получить после запуска, чтобы не использовать root */
    const char* group; /**< имя группы которую нужно использовать после запуска */
#ifdef LTRD_DAEMON_ENABLED
    const char* pid_file; /**< pid-файл (используется для отслеживания повторного создания демона) */
#endif
#ifdef LTRD_SYSTEMD_ENABLED
    unsigned wdt_tout;
#endif
} t_ltrsrv_opts;

extern t_ltrsrv_opts g_opts;

int ltrsrv_parse_opt(t_ltrsrv_opts* opts, int argc, char** argv);

#endif // LTRSRV_OPT_PARSE_H
