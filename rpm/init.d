#!/bin/sh
### BEGIN INIT INFO
# Provides:          ltrd
# Required-Start:    $network $local_fs $remote_fs
# Required-Stop:     $local_fs $remote_fs
# Default-Start:     2 3 5
# Default-Stop:      0 1 6
# Short-Description: daemon for control LCard LTR-Crates
# Description:       ltrd control LTR-Crates. ltrapi uses ltrd for make all 	  
#                    operatios with LTR-crates and LTR-modules.
### END INIT INFO

# Author: Borisov Alexey <borisov@lcard.ru>


# PATH should only include /usr/ if it runs after the mountnfs.sh script
PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="LTR-Crates control daemon"     # Introduce a short description here
NAME=ltrd                          # Introduce the short server's name here
DAEMON=/usr/bin/ltrd               # Introduce the server's location here
PIDFILE_DIR=/var/run/$NAME
PIDFILE=$PIDFILE_DIR/pid
SCRIPTNAME=/etc/init.d/$NAME
CONF_FILE=/etc/ltrd/config.xml
USER=ltrd
DAEMON_ARGS="--config-file=$CONF_FILE --pid-file=$PIDFILE"   # Arguments to run the daemon with


# Check for missing binaries (stale symlinks should not happen)
# Note: Special treatment of stop for LSB conformance
test -x $DAEMON || { echo "$DAEMON not installed"; 
	if [ "$1" = "stop" ]; then exit 0;
	else exit 5; fi; }



# Read configuration variable file if it is present
[ -r /etc/sysconfig/$NAME ] && . /etc/sysconfig/$NAME

# Use the SUSE rc_ init script functions;
# emulate them on LSB, RH and other systems

# Default: Assume sysvinit binaries exist
start_daemon() { /sbin/start_daemon ${1+"$@"}; }
killproc()     { /sbin/killproc     ${1+"$@"}; }
pidofproc()    { /sbin/pidofproc    ${1+"$@"}; }
checkproc()    { /sbin/checkproc    ${1+"$@"}; }
if test -e /etc/rc.status; then
    # SUSE rc script library
    . /etc/rc.status
else
    export LC_ALL=POSIX
    _cmd=$1
    declare -a _SMSG
    if test "${_cmd}" = "status"; then
	_SMSG=(running dead dead unused unknown reserved)
	_RC_UNUSED=3
    else
	_SMSG=(done failed failed missed failed skipped unused failed failed reserved)
	_RC_UNUSED=6
    fi
    if test -e /lib/lsb/init-functions; then
	# LSB    
    	. /lib/lsb/init-functions
	echo_rc()
	{
	    if test ${_RC_RV} = 0; then
		log_success_msg "  [${_SMSG[${_RC_RV}]}] "
	    else
		log_failure_msg "  [${_SMSG[${_RC_RV}]}] "
	    fi
	}
	# TODO: Add checking for lockfiles
	checkproc() { return pidofproc ${1+"$@"} >/dev/null 2>&1; }
    elif test -e /etc/init.d/functions; then
	# RHAT
	. /etc/init.d/functions
	echo_rc()
	{
	    #echo -n "  [${_SMSG[${_RC_RV}]}] "
	    if test ${_RC_RV} = 0; then
		success "  [${_SMSG[${_RC_RV}]}] "
	    else
		failure "  [${_SMSG[${_RC_RV}]}] "
	    fi
	}
	checkproc() { return status ${1+"$@"}; }
	start_daemon() { return daemon ${1+"$@"}; }
    else
	# emulate it
	echo_rc() { echo "  [${_SMSG[${_RC_RV}]}] "; }
    fi
    rc_reset() { _RC_RV=0; }
    rc_failed()
    {
	if test -z "$1"; then 
	    _RC_RV=1;
	elif test "$1" != "0"; then 
	    _RC_RV=$1; 
    	fi
	return ${_RC_RV}
    }
    rc_check()
    {
	return rc_failed $?
    }	
    rc_status()
    {
	rc_failed $?
	if test "$1" = "-r"; then _RC_RV=0; shift; fi
	if test "$1" = "-s"; then rc_failed 5; echo_rc; rc_failed 3; shift; fi
	if test "$1" = "-u"; then rc_failed ${_RC_UNUSED}; echo_rc; rc_failed 3; shift; fi
	if test "$1" = "-v"; then echo_rc; shift; fi
	if test "$1" = "-r"; then _RC_RV=0; shift; fi
	return ${_RC_RV}
    }
    rc_exit() { exit ${_RC_RV}; }
    rc_active() 
    {
	if test -z "$RUNLEVEL"; then read RUNLEVEL REST < /sbin/runlevel; fi
	if test -e /etc/init.d/S[0-9][0-9]${1}; then return 0; fi
	return 1
    }
fi

# Reset status of this service
rc_reset



case "$1" in
    start)
	echo -n "Starting $NAME"
	## Start daemon with startproc(8). If this fails
	## the return value is set appropriately by startproc.
	start_daemon -p $PIDFILE -u ltrd -g ltrd $DAEMON $DAEMON_ARGS

	# Remember status and be verbose
	rc_status -v
	;;
    stop)
	echo -n "Shutting down $NAME "
	## Stop daemon with killproc(8) and if this fails
	## killproc sets the return value according to LSB.

	killproc $DAEMON

	# Remember status and be verbose
	rc_status -v
	;;
    try-restart|condrestart)
	## Do a restart only if the service was active before.
	## Note: try-restart is now part of LSB (as of 1.9).
	## RH has a similar command named condrestart.
	if test "$1" = "condrestart"; then
		echo "${attn} Use try-restart ${done}(LSB)${attn} rather than condrestart ${warn}(RH)${norm}"
	fi
	$0 status
	if test $? = 0; then
		$0 restart
	else
		rc_reset	# Not running is not a failure.
	fi
	# Remember status and be quiet
	rc_status
	;;
    restart)
	## Stop the service and regardless of whether it was
	## running or not, start it again.
	$0 stop
	$0 start

	# Remember status and be quiet
	rc_status
	;;
    force-reload)
	## Signal the daemon to reload its config. Most daemons
	## do this on signal 1 (SIGHUP).
	## If it does not support it, restart the service if it
	## is running.

	echo -n "Reload service $NAME "
	## if it supports it:
	killproc -HUP $DAEMON
	#touch /var/run/FOO.pid
	rc_status -v

	## Otherwise:
	#$0 try-restart
	#rc_status
	;;
    reload)
	## Like force-reload, but if daemon does not support
	## signaling, do nothing (!)

	# If it supports signaling:
	echo -n "Reload service $NAME "
	killproc -HUP $FOO_BIN
	#touch /var/run/FOO.pid
	rc_status -v
	
	## Otherwise if it does not support reload:
	#rc_failed 3
	#rc_status -v
	;;
    status)
	echo -n "Checking for service $NAME "
	## Check status with checkproc(8), if process is running
	## checkproc will return with exit status 0.

	# Return value is slightly different for the status command:
	# 0 - service up and running
	# 1 - service dead, but /var/run/  pid  file exists
	# 2 - service dead, but /var/lock/ lock file exists
	# 3 - service not running (unused)
	# 4 - service status unknown :-(
	# 5--199 reserved (5--99 LSB, 100--149 distro, 150--199 appl.)
	
	# NOTE: checkproc returns LSB compliant status values.
	checkproc $DAEMON
	# NOTE: rc_status knows that we called this init script with
	# "status" option and adapts its messages accordingly.
	rc_status -v
	;;
    probe)
	## Optional: Probe for the necessity of a reload, print out the
	## argument to this init script which is required for a reload.
	## Note: probe is not (yet) part of LSB (as of 1.9)

	test $CONF_FILE -nt $PIDFILE && echo reload
	;;
    *)
	echo "Usage: $0 {start|stop|status|try-restart|restart|force-reload|reload|probe}"
	exit 1
	;;
esac
rc_exit